package gomodelbinding

import "testing"

func TestParseToNullTime(t *testing.T) {
	timeLocCases := []struct {
		time string
		locs []string
	}{
		{"2017-1-18", []string{"Japan", ""}},
		{"2017-01-18", []string{"Japan", ""}},
		{"2017-10-1", []string{"Japan", ""}},
		{"2017-10-01", []string{"Japan", ""}},
		{"2017-12-18", []string{"Japan", ""}},
		{"2017-1-18 11:22:33", []string{"Japan", ""}},
		{"2017-01-18 11:22:33", []string{"Japan", ""}},
		{"2017-12-1 11:22:33", []string{"Japan", ""}},
		{"2017-12-01 11:22:33", []string{"Japan", ""}},
		{"2017-1-18 1:2:3", []string{"Japan", ""}},
		{"2017-01-18 01:02:03", []string{"Japan", ""}},
		{"2017-1-18 11:22:33.000", []string{"Japan", ""}},
		{"2017-01-18 11:22:33.000", []string{"Japan", ""}},
		{"2017-12-1 11:22:33.000", []string{"Japan", ""}},
		{"2017-12-01 11:22:33.000", []string{"Japan", ""}},
		{"2017-1-18 1:2:3.000", []string{"Japan", ""}},
		{"2017-01-18 01:02:03.000", []string{"Japan", ""}},
		{"2017-1-18T11:22:33Z", []string{"Japan", ""}},
		{"2017-01-18T11:22:33Z", []string{"Japan", ""}},
		{"2017-12-1T11:22:33Z", []string{"Japan", ""}},
		{"2017-12-01T11:22:33Z", []string{"Japan", ""}},
		{"2017-1-19T1:2:3Z", []string{"Japan", ""}},
		{"2017-01-19T1:2:3Z", []string{"Japan", ""}},
		{"2017-10-1T11:22:33.000Z", []string{"Japan", ""}},
		{"2017-1-18T11:22:33+09:00", []string{""}},
		{"2017-1-19T1:2:3+09:00", []string{""}},
		{"2017-10-1T11:22:33.000+09:00", []string{}},
		{"2017/1/18", []string{"Japan", ""}},
		{"2017/01/18", []string{"Japan", ""}},
		{"2017/10/1", []string{"Japan", ""}},
		{"2017/10/01", []string{"Japan", ""}},
		{"2017/12/18", []string{"Japan", ""}},
		{"2017/1/18 11:22:33", []string{"Japan", ""}},
		{"2017/01/18 11:22:33", []string{"Japan", ""}},
		{"2017/12/1 11:22:33", []string{"Japan", ""}},
		{"2017/12/01 11:22:33", []string{"Japan", ""}},
	}

	for i, c := range timeLocCases {
		for j, loc := range c.locs {
			nullTime := ParseToNullTime(c.time, loc)
			if nullTime.Error() != nil {
				t.Errorf("[TimeCase:%d,LocCase:%d] parse error : %+v \r\n", i+1, j+1, nullTime.Error())
				return
			}
			if !nullTime.Valid {
				t.Errorf("[TimeCase:%d,LocCase:%d] invalid %v \r\n", i+1, j+1, nullTime.Time)
				return
			}
			t.Logf("success : %+v \r\n", nullTime)
		}
	}
}
