// +build integration

package gomodelbinding

import (
	"database/sql"
	"testing"

	_ "github.com/denisenkom/go-mssqldb"
)

func TestNullInt64Wrapper(t *testing.T) {

	var db *sql.DB
	var err error
	if db, err = sql.Open("mssql", "server=localhost;user id=sa;password=12345678;database=GOMODELBIND_TEST"); err != nil {
		t.Errorf("コネクション確立に失敗: %+v", err)
		return
	}

	var tx *sql.Tx
	if tx, err = db.Begin(); err != nil {
		t.Errorf("トランザクション開始に失敗: %+v", err)
		return
	}

	sqlStrs := []string{
		"SELECT TOP 1 RECORD_NO FROM dbo.HO_TABLE WHERE P_NO = '0002001' ORDER BY RECORD_NO DESC",
		"SELECT TOP 1 AAAA FROM dbo.HO_TABLE WHERE P_NO = '0002001' ORDER BY RECORD_NO DESC",
		"SELECT BBBB FROM dbo.HO_TABLE WHERE RECORD_NO = 191919",
	}

	for i, sqlStr := range sqlStrs {
		var rows *sql.Rows
		rows, err = tx.Query(sqlStr)
		if err != nil {
			t.Errorf("No.%d クエリ発行に失敗: %+v", i+1, err)
			return
		}
		w := newNullInt64Wrapper()
		if rows.Next() {
			err = rows.Scan(w)
			if err != nil {
				t.Errorf("No.%d データ読み込みに失敗: %+v", i+1, err)
				return
			}
		}
		t.Logf("No.%d 読込した値  valid:%v, value:%v", i+1, w.valid(), w.value())
	}

}

func TestNullStringWrapper(t *testing.T) {
	var db *sql.DB
	var err error
	if db, err = sql.Open("mssql", "server=localhost;user id=sa;password=12345678;database=GOMODELBIND_TEST"); err != nil {
		t.Errorf("コネクション確立に失敗: %+v", err)
		return
	}

	var tx *sql.Tx
	if tx, err = db.Begin(); err != nil {
		t.Errorf("トランザクション開始に失敗: %+v", err)
		return
	}

	sqlStrs := []string{
		"SELECT P_NO FROM dbo.HO_TABLE WHERE RECORD_NO = 1094",
		"SELECT AAAA FROM dbo.HO_TABLE WHERE RECORD_NO = 1094",
		"SELECT P_NO FROM dbo.HO_TABLE WHERE RECORD_NO = 191919",
	}

	for i, sqlStr := range sqlStrs {
		var rows *sql.Rows
		rows, err = tx.Query(sqlStr)
		if err != nil {
			t.Errorf("No.%d クエリ発行に失敗: %+v", i+1, err)
			return
		}
		w := newNullStringWrapper()
		if rows.Next() {
			err = rows.Scan(w)
			if err != nil {
				t.Errorf("No.%d データ読み込みに失敗: %+v", i+1, err)
				return
			}
		}
		t.Logf("No.%d 読込した値  valid:%v, value:%v", i+1, w.valid(), w.value())
	}
}
