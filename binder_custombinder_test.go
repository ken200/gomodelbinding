package gomodelbinding

import (
	"reflect"
	"testing"
	"time"
)

type testModel struct {
	ID     string `bindmap:"col:id,type:varchar"`
	Name   string `bindmap:"col:name,type:varchar"`
	Age    int
	gotest *testing.T
}

func (m *testModel) Bind(sdests ...interface{}) {

	//m.gotest.Logf("Bind() target is %+v \r\n", sdests[2])

	v, ok := sdests[2].(FieldBinder)
	if !ok {
		//m.gotest.Logf("Bind() return A (%v) \r\n", v)
		return
	}
	if !v.Valid() {
		//m.gotest.Logf("Bind() return B (%+v) \r\n", v.value())
		return
	}
	var birthdayStr string
	if birthdayStr, ok = v.Value().(string); !ok {
		//m.gotest.Logf("Bind() return C (%+v) \r\n", v.value())
		return
	}
	birthday, err := time.Parse("2006-1-2 15:4:5", birthdayStr)
	if err != nil {
		//m.gotest.Logf("Bind() return D (%+v) \r\n", err)
		return
	}

	m.Age = birthday.Year() - 2000
}

func TestBindUsingCustomBinder(t *testing.T) {

	model := &testModel{gotest: t}

	//dests各要素への値セットはsql.Scan()による値読込みを表す
	var dests = make([]interface{}, 0)
	if w := newNullStringWrapper(); true {
		w.real.String = "0002001"
		w.real.Valid = true
		dests = append(dests, w)
	}
	if w := newNullStringWrapper(); true {
		w.real.String = "山田太郎"
		w.real.Valid = true
		dests = append(dests, w)
	}
	if w := newNullStringWrapper(); true {
		w.real.String = "2017-1-16 11:22:33"
		w.real.Valid = true
		dests = append(dests, w)
	}

	customBind(model, dests)

	if v := dests[0].(FieldBinder).Value(); reflect.DeepEqual(model.ID, v) {
		t.Errorf("bind unmatch (ID) : expect is %v but result is %v", v, model.ID)
	}

	if v := dests[1].(FieldBinder).Value(); reflect.DeepEqual(model.Name, v) {
		t.Errorf("bind unmatch (Name) : expect is %v but result is %v", v, model.Name)
	}

	if model.Age != 17 {
		t.Errorf("bind unmatch (Age) : expect is 20 but result is %v", model.Age)
	}
}
