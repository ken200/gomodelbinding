package gomodelbinding

import "time"
import "database/sql"

import "errors"

import "fmt"

type ValueGetter interface {
	Value() interface{}
}

type NullTime struct {
	Valid bool
	Time  time.Time
	error error
}

func (nt *NullTime) Value() interface{} {
	if nt.Valid {
		return nt.Time
	}
	return nil
}

func (nt *NullTime) Error() error {
	return nt.error
}

func (nt *NullTime) MarshalJSON() ([]byte, error) {
	json := make([]byte, 0)
	json = append(json, []byte(`{`)...)
	json = append(json, []byte(fmt.Sprintf(`"hasValue": %v,`, nt.Valid))...)
	json = append(json, []byte(fmt.Sprintf(`"value": "%s"`, nt.Time.Format("2006-01-02 15:04:05")))...)
	json = append(json, []byte(`}`)...)
	return json, nil
}

func (nt *NullTime) SetLocationWithoutCorrectionOffset(locName string) *NullTime {
	if loc, err := time.LoadLocation(locName); err != nil {
		nt.error = err
	} else {
		nt.Time = nt.Time.In(loc)
		_, offsetSec := nt.Time.Zone()
		nt.Time = nt.Time.Add(time.Duration(offsetSec) * time.Second * -1)
	}
	return nt
}

func ParseToNullTime(val interface{}, locName string) *NullTime {
	conv := func(v sql.NullString) *NullTime {
		ret := &NullTime{Valid: false, error: errors.New("noval")}
		if v.Valid {
			layouts := []string{"2006-1-2 15:4:5.000 -0700 MST", "2006-1-2T15:4:5Z", "2006-1-2T15:4:5-07:00", "2006-1-2T15:4:5.000-07:00", "2006-1-2", "2006-1-2 15:4:5", "2006/1/2", "2006/1/2 15:4:5"}
			for _, layout := range layouts {
				if t, err := time.Parse(layout, v.String); err == nil {
					ret.Valid = true
					ret.Time = t
					ret.error = nil
					break
				} else {
					ret.error = err
				}
			}
			if locName != "" {
				ret = ret.SetLocationWithoutCorrectionOffset(locName)
			}
		}
		return ret
	}
	if v, ok := val.(*sql.NullString); ok {
		return conv(*v)
	}
	if v, ok := val.(sql.NullString); ok {
		return conv(v)
	}
	if v, ok := val.(string); ok {
		return conv(sql.NullString{String: v, Valid: true})
	}
	return conv(sql.NullString{Valid: false})
}

func NewNullTime(value time.Time) *NullTime {
	ret := &NullTime{Valid: false, error: errors.New("noval")}
	if value.Format(time.RFC3339Nano) != "0001-01-01T00:00:00Z" {
		ret.Valid = true
		ret.error = nil
		ret.Time = value
	}
	return ret
}

type NullInt64 struct {
	Valid bool  `json:"hasValue"`
	Int64 int64 `json:"value"`
	error error
}

func (ni64 *NullInt64) Value() interface{} {
	if ni64.Valid {
		return ni64.Int64
	}
	return nil
}

func (ni64 *NullInt64) Error() error {
	return ni64.error
}

func NewNullIn64(value int64) *NullInt64 {
	return &NullInt64{
		Valid: true,
		Int64: value,
		error: nil,
	}
}

func ParseToNullInt64FromSqlNullInt64(val interface{}) *NullInt64 {
	conv := func(v sql.NullInt64) *NullInt64 {
		ret := &NullInt64{Valid: false, error: errors.New("noval")}
		if v.Valid {
			ret.Valid = true
			ret.Int64 = v.Int64
			ret.error = nil
		}
		return ret
	}
	if v, ok := val.(*sql.NullInt64); ok {
		return conv(*v)
	}
	if v, ok := val.(sql.NullInt64); ok {
		return conv(v)
	}
	return conv(sql.NullInt64{Valid: false})
}

// func ParseToNullInt64FromSqlNullString(value sql.NullString) NullInt64 {
// 	ret := NullInt64{Valid: false, error: errors.New("noval")}
// 	if value.Valid {
// 		if num, err := strconv.ParseInt(value.String, 10, 64); err == nil {
// 			ret.Valid = true
// 			ret.Int64 = num
// 			ret.error = nil
// 		} else {
// 			ret.error = err
// 		}
// 	}
// 	return ret
// }

type NullFloat64 struct {
	Valid   bool    `json:"hasValue"`
	Float64 float64 `json:"value"`
	error   error
}

func (fl64 *NullFloat64) Value() interface{} {
	if fl64.Valid {
		return fl64.Float64
	}
	return nil
}

func (fl64 *NullFloat64) Error() error {
	return fl64.error
}

func NewNullFloat64(value float64) *NullFloat64 {
	return &NullFloat64{
		Valid:   true,
		Float64: value,
		error:   nil,
	}
}
