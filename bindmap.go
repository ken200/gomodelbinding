package gomodelbinding

import "strings"
import "reflect"
import "errors"
import "unicode"

type BindMapColumnNameAutoConvertKind int

const (
	CamelToSnake BindMapColumnNameAutoConvertKind = iota
	SnakeToCamel
	NonConvert
)

var CurrentBindMapColumnNameAutoConvertKind = CamelToSnake

var NonBindMapField = errors.New("this is non bindmap field")

type bindMap struct {
	BindColName string
	BindColType string
}

func (bm *bindMap) dest() (result FieldBinder) {
	switch bm.BindColType {
	case "int":
		result = newNullInt64Wrapper()
	case "datetime":
		result = newNullTimeWrapper()
	case "float":
		result = newNullFloat64Wrapper()
	default:
		result = newNullStringWrapper()
	}
	return
}

func newBindMap(f reflect.StructField) (result *bindMap, err error) {
	result = new(bindMap)
	result.BindColName, err = parseColName(f)
	if err != nil {
		return
	}
	result.BindColType, err = parseColType(f)
	if err != nil {
		return
	}
	return
}

func parseColName(f reflect.StructField) (result string, err error) {
	tag, ok := f.Tag.Lookup("bindmap")
	if !ok {
		result = autoConvertColName(f)
		return
	}
	fs := strings.Split(tag, ",")
	for _, f := range fs {
		if strings.ToLower(f) == "nobind" {
			err = NonBindMapField
			return
		}
		kv := strings.Split(f, ":")
		if strings.ToLower(kv[0]) == "col" {
			result = kv[1]
			return
		}
	}
	return
}

func autoConvertColName(f reflect.StructField) string {
	switch CurrentBindMapColumnNameAutoConvertKind {
	case CamelToSnake:
		return cnvCamelToSnake(f.Name)
	case SnakeToCamel:
		return cnvSnakeToCamel(f.Name)
	default:
		return f.Name
	}
}

func cnvCamelToSnake(camel string) string {
	var snake []rune
	cnvSkip := 0
	for i, c := range camel {
		cnvSkip = cnvSkip - 1
		if unicode.IsUpper(c) {
			if i > 0 && cnvSkip <= 0 {
				snake = append(snake, '_')
			}
			snake = append(snake, unicode.ToLower(c))
			if len(camel)-1 > i && strings.ToLower(camel[i:i+2]) == "id" {
				cnvSkip = 2
			}
			continue
		}
		snake = append(snake, c)
	}
	return string(snake)
}

func cnvSnakeToCamel(snake string) string {
	var camel []rune
	upperFlg := false
	for i, s := range snake {
		if i == 0 {
			camel = append(camel, unicode.ToUpper(s))
			continue
		}
		if s == '_' {
			upperFlg = true
			continue
		}
		if upperFlg {
			camel = append(camel, unicode.ToUpper(s))
			upperFlg = false
			continue
		}
		camel = append(camel, unicode.ToLower(s))
	}
	return string(camel)
}

func parseColType(f reflect.StructField) (result string, err error) {
	tag, ok := f.Tag.Lookup("bindmap")
	if !ok {
		t := f.Type
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		typ := t.Name()
		if strings.HasSuffix(typ, "NullTime") {
			result = "datetime"
			return
		}
		if strings.HasSuffix(typ, "NullInt64") {
			result = "int"
			return
		}
		if strings.HasSuffix(typ, "NullFloat64") {
			result = "float"
			return
		}
		result = "string"
		return
	}
	fs := strings.Split(tag, ",")
	for _, f := range fs {
		if strings.ToLower(f) == "nobind" {
			err = NonBindMapField
			return
		}
		kv := strings.Split(f, ":")
		if strings.ToLower(kv[0]) == "type" {
			result = kv[1]
			return
		}
	}
	return
}
