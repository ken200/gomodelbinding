package gomodelbinding

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

var extractColsPart = regexp.MustCompile(`^select\s{1,}([^\*]+?)\s{1,}from`)

func splitCols(selectSql string) (result []string, err error) {
	selectSql = strings.ToLower(selectSql)
	selectSql = strings.Replace(selectSql, "\r\n", " ", -1)
	selectSql = strings.Replace(selectSql, "\r", " ", -1)
	selectSql = strings.Replace(selectSql, "\n", " ", -1)

	extractResult := extractColsPart.FindStringSubmatch(selectSql)
	if len(extractResult) < 2 {
		err = errors.New("the select query is not valid format")
		return
	}

	cols := extractResult[1]
	if strings.Contains(cols, "*") {
		err = errors.New("can not use asterisk in select query")
		return
	}

	colsAry := strings.Split(cols, ",")
	result = make([]string, len(colsAry))
	for i, c := range colsAry {
		if strings.Contains(c, " as ") {
			result[i] = strings.TrimSpace(strings.Split(c, "as")[1])
			continue
		}
		if strings.Contains(c, ".") {
			result[i] = strings.TrimSpace(strings.Split(c, ".")[1])
			continue
		}
		result[i] = strings.TrimSpace(c)
	}
	return
}

//destは、フィールド毎のfieldBinder。
//fieldIdxは、destが対応するフィールドインデックス。dest同一位置要素と連携している。対応するフィールドが存在しない列の場合は-1がセットされている。
func generateScanDest(model interface{}, tableColName string) (dest interface{}, fieldIdx int) {
	dest = newNullStringWrapper()
	fieldIdx = -1
	t := reflect.TypeOf(model).Elem()
	for i := 0; i < t.NumField(); i++ {
		bmap, err := newBindMap(t.Field(i))
		if err == NonBindMapField {
			continue
		}
		if strings.ToLower(bmap.BindColName) != tableColName {
			continue
		}
		dest = bmap.dest()
		fieldIdx = i
		return
	}
	return
}

func generateScanDests(cols []string, model interface{}) (dests []interface{}, fieldIdxs []int) {
	dests = make([]interface{}, 0)
	fieldIdxs = make([]int, 0)
	for _, c := range cols {
		dest, fIdx := generateScanDest(model, c)
		fieldIdxs = append(fieldIdxs, fIdx)
		dests = append(dests, dest)
	}
	return
}

func bind(dests []interface{}, fieldIdxs []int, model interface{}) (err error) {
	rv := reflect.ValueOf(model).Elem()
	for i, d := range dests {
		fIdx := fieldIdxs[i]
		if fIdx == -1 {
			continue
		}
		f := rv.Field(fIdx)
		b, ok := d.(FieldBinder)
		if !ok {
			err = fmt.Errorf("dests parameter of bind() need implementing fieldBinder interface. dests[%d] is not implementing", i)
			return
		}
		f.Set(reflect.ValueOf(b.Value()))
	}
	return
}

func customBind(model interface{}, sdests []interface{}) {
	customBinder, ok := model.(CustomBinder)
	if ok {
		customBinder.Bind(sdests...)
	}
}

type CustomBinder interface {
	Bind(sdests ...interface{})
}

func Fetch(tx *sql.Tx, modelGen func() interface{}, selectSql string, args ...interface{}) (result []interface{}, err error) {
	result = make([]interface{}, 0)

	defer func() {
		CurrentBindMapColumnNameAutoConvertKind = CamelToSnake
	}()

	var cols []string
	cols, err = splitCols(selectSql)
	if err != nil {
		return
	}

	var rows *sql.Rows
	rows, err = tx.Query(selectSql, args...)
	if err != nil {
		return
	}

	for rows.Next() {
		model := modelGen()
		sdests, fieldIdxs := generateScanDests(cols, model)
		err = rows.Scan(sdests...)
		if err != nil {
			return
		}
		err = bind(sdests, fieldIdxs, model)
		if err != nil {
			return
		}
		customBind(model, sdests)
		result = append(result, model)
	}

	return
}
