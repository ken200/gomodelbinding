gomodelbinding
===

sql.Scan()周辺のバインド処理を行う

### コード例

```
package models

import (
	"database/sql"

	modelbinding "bitbucket.org/ken200/gomodelbinding"
	"github.com/pkg/errors"
)

type Physical struct {
	ID                    string
	DateTime              *modelbinding.NullTime    `bindmap:"col:register_date,type:datetime"`
	Weight                *modelbinding.NullFloat64
	BodyFat               *modelbinding.NullFloat64
	Memo                  string
	JoggingCourse         string
	JoggingKm             *modelbinding.NullFloat64
	JoggingResultHours    *modelbinding.NullInt64
	JoggingResultMinuts   *modelbinding.NullInt64
	JoggingResultSeconds  *modelbinding.NullInt64
	JoggingResultMSeconds *modelbinding.NullInt64
	JoggingMemo           string
}

func FetchPhysical(tx *sql.Tx) (result []interface{}, err error) {

	var sqlstr = `SELECT
	id,
	register_date,
	weight,
	body_fat,
	memo,
	jogging_course,
	jogging_km,
	jogging_result_hours,
	jogging_result_minuts,
	jogging_result_seconds,
	jogging_result_m_seconds,
	jogging_memo
	FROM
		dbo.PersonalPhysicalRecords
	WHERE
		delete_flg = 0
	ORDER BY
		register_date desc
	`
	result, err = modelbinding.Fetch(tx, func() interface{} { return &Physical{} }, sqlstr)
	if err != nil {
		err = errors.WithStack(err)
	}
	return
}
```