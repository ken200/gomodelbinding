package gomodelbinding

import "database/sql"
import "strconv"

type FieldBinder interface {
	sql.Scanner
	Value() interface{}
	Valid() bool
}

//newNullInt64Wrapperはsql.NullInt64をバインド用に共通インターフェイスをラップしたもの
type nullInt64Wrapper struct {
	real *NullInt64
}

func newNullInt64Wrapper() *nullInt64Wrapper {
	return &nullInt64Wrapper{real: &NullInt64{}}
}

func (w *nullInt64Wrapper) Scan(src interface{}) error {
	ns := &sql.NullString{}
	err := ns.Scan(src)
	if err == nil {
		num, err := strconv.ParseInt(ns.String, 10, 64)
		if err == nil {
			w.real = NewNullIn64(num)
		}
	}
	return err
}

func (w *nullInt64Wrapper) Value() interface{} {
	return w.real
}

func (w *nullInt64Wrapper) Valid() bool {
	return w.real.Valid
}

//nullStringWrapperはsql.NullStringをバインド用に共通インターフェイスをラップしたもの
type nullStringWrapper struct {
	real *sql.NullString
}

func newNullStringWrapper() *nullStringWrapper {
	return &nullStringWrapper{real: &sql.NullString{}}
}

func (w *nullStringWrapper) Scan(src interface{}) error {
	return w.real.Scan(src)
}

func (w *nullStringWrapper) Value() interface{} {
	return w.real.String
}

func (w *nullStringWrapper) Valid() bool {
	return w.real.Valid
}

//nullTimeWrapperは日付バインド処理を共通インターフェイスをラップしたもの
type nullTimeWrapper struct {
	real *NullTime
}

func newNullTimeWrapper() *nullTimeWrapper {
	return &nullTimeWrapper{real: &NullTime{}}
}

func (w *nullTimeWrapper) Scan(src interface{}) error {
	ns := &sql.NullString{}
	err := ns.Scan(src)
	if err == nil {
		w.real = ParseToNullTime(ns, "Japan")
	}
	return err
}

func (w *nullTimeWrapper) Value() interface{} {
	return w.real
}

func (w *nullTimeWrapper) Valid() bool {
	return w.real.Valid
}

//nullFloat64Wrapperはsql.NullFloat64をバインド用に共通インターフェイスをラップしたもの
type nullFloat64Wrapper struct {
	real *NullFloat64
}

func newNullFloat64Wrapper() *nullFloat64Wrapper {
	return &nullFloat64Wrapper{real: &NullFloat64{}}
}

func (w *nullFloat64Wrapper) Scan(src interface{}) error {
	ns := &sql.NullString{}
	err := ns.Scan(src)
	if err == nil {
		num, err := strconv.ParseFloat(ns.String, 64)
		if err == nil {
			w.real = NewNullFloat64(num)
		}
	}
	return err
}

func (w *nullFloat64Wrapper) Value() interface{} {
	return w.real
}

func (w *nullFloat64Wrapper) Valid() bool {
	return w.real.Valid
}
