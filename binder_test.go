package gomodelbinding

import (
	"reflect"
	"testing"
	"time"
)

func TestSplitCols(t *testing.T) {
	cases := []string{
		`SELECT 
t1.HOGE_NO AS HOGEGE_NO,
t2.RECORD_NO,
NAME_KANJI,
NAME_KANA AS HURIGANA 
FROM 
HO_TABLE ...`,
		`SELECT 
	t1.HOGE_NO AS HOGEGE_NO, 
	t2.RECORD_NO, 
	NAME_KANJI, 
	NAME_KANA AS HURIGANA 
FROM 
	HO_TABLE ...`,
		`SELECT t1.HOGE_NO AS HOGEGE_NO,t2.RECORD_NO,NAME_KANJI,NAME_KANA AS HURIGANA FROM HO_TABLE ...`,
		`SELECT t1.HOGE_NO AS HOGEGE_NO, t2.RECORD_NO, NAME_KANJI, NAME_KANA AS HURIGANA FROM HO_TABLE ...`,
	}

	expectCols := []string{"hogege_no", "record_no", "name_kanji", "hurigana"}

	for i, sqlStr := range cases {
		cols, err := splitCols(sqlStr)
		if err != nil {
			t.Errorf("No.%d splitCols() error : %+v \r\n", i+1, err)
			return
		}
		if !reflect.DeepEqual(expectCols, cols) {
			t.Errorf("No.%d unmatch : expect is %+v but result is %+v \r\n", i+1, expectCols, cols)
			return
		}
	}
}

func TestGenerateScanDests(t *testing.T) {
	model := &struct {
		TestID string    `bindmap:"col:id,type:varchar"`
		Date   time.Time `bindmap:"col:date_time,type:datetime"`
		Age    int       `bindmap:"col:the_age,type:int"`
	}{}

	caseAndexpects := []struct {
		ColNames []string
		Expects  []reflect.Type
	}{
		{[]string{"id", "date_time", "the_age"}, []reflect.Type{reflect.TypeOf(newNullStringWrapper()), reflect.TypeOf(newNullTimeWrapper()), reflect.TypeOf(newNullInt64Wrapper())}},
		{[]string{"date_time", "id", "the_age"}, []reflect.Type{reflect.TypeOf(newNullTimeWrapper()), reflect.TypeOf(newNullStringWrapper()), reflect.TypeOf(newNullInt64Wrapper())}},
	}

	for i, cae := range caseAndexpects {
		dests, _ := generateScanDests(cae.ColNames, model)
		if len(cae.Expects) != len(dests) {
			t.Errorf("expects length is %d but result length is %d \r\n", len(cae.Expects), len(dests))
			return
		}
		for j, d := range dests {
			rt := reflect.TypeOf(d)
			if cae.Expects[j] != rt {
				t.Errorf("No.%d type is not match : expect is %+v but result is %+v \r\n", i+1, cae.Expects[j], rt)
				return
			}
		}
	}
}

func TestGenerateScanDests_ExistNonBindField(t *testing.T) {
	model := &struct {
		TestID string `bindmap:"col:id,type:varchar"`
		Date   time.Time
		Age    int `bindmap:"col:the_age,type:int"`
	}{}

	caseAndexpects := []struct {
		ColNames []string
		Expects  []reflect.Type
	}{
		{[]string{"id", "the_age"}, []reflect.Type{reflect.TypeOf(newNullStringWrapper()), reflect.TypeOf(newNullInt64Wrapper())}},
		{[]string{"the_age", "id"}, []reflect.Type{reflect.TypeOf(newNullInt64Wrapper()), reflect.TypeOf(newNullStringWrapper())}},
	}

	for i, cae := range caseAndexpects {
		dests, _ := generateScanDests(cae.ColNames, model)
		if len(cae.Expects) != len(dests) {
			t.Errorf("expects length is %d but result length is %d \r\n", len(cae.Expects), len(dests))
			return
		}
		for j, d := range dests {
			rt := reflect.TypeOf(d)
			if cae.Expects[j] != rt {
				t.Errorf("No.%d type is not match : expect is %+v but result is %+v \r\n", i+1, cae.Expects[j], rt)
				return
			}
		}
	}
}

func TestGenerateScanDests_UnmatchLengthOfColumnAndBinder(t *testing.T) {
	model := &struct {
		TestID string `bindmap:"col:id,type:varchar"`
		Date   time.Time
		Age    int `bindmap:"col:the_age,type:int"`
	}{}

	colNames := []string{"id", "hoge", "the_age"}
	expectDestTypes := []reflect.Type{reflect.TypeOf(newNullStringWrapper()), reflect.TypeOf(newNullStringWrapper()), reflect.TypeOf(newNullInt64Wrapper())}
	expectFieldIdxs := []int{0, -1, 2}

	dests, fieldIdxs := generateScanDests(colNames, model)

	if len(colNames) != len(dests) {
		t.Errorf("expects length is %d but result length is %d \r\n", len(colNames), len(dests))
		return
	}

	for i, _ := range colNames {
		if expectDestTypes[i] != reflect.TypeOf(dests[i]) {
			t.Errorf("No.%d type is not match : expect is %+v but result is %+v \r\n", i+1, expectDestTypes[i], reflect.TypeOf(dests[i]))
			return
		}
		if expectFieldIdxs[i] != fieldIdxs[i] {
			t.Errorf("No.%d fieldIndex is not match : expect is %d but result is %d \r\n", i+1, expectFieldIdxs[i], fieldIdxs[i])
			return
		}
	}
}

func TestBind(t *testing.T) {
	model := &struct {
		TestID string     `bindmap:"col:id,type:varchar"`
		Date   *NullTime  `bindmap:"col:date_time,type:datetime"`
		Age    *NullInt64 `bindmap:"col:the_age,type:int"`
	}{}

	//dests各要素への値セットはsql.Scan()による値読込みを表す
	var dests = make([]interface{}, 0)
	if w := newNullStringWrapper(); true {
		w.Scan("999999")
		dests = append(dests, w)
	}
	if w := newNullTimeWrapper(); true {
		w.Scan("2017-1-16 11:22:33")
		dests = append(dests, w)
	}
	if w := newNullInt64Wrapper(); true {
		w.Scan(1000)
		dests = append(dests, w)
	}

	fieldIdxs := []int{0, 1, 2}

	err := bind(dests, fieldIdxs, model)
	if err != nil {
		t.Errorf("bind error : %+v", err)
		return
	}

	if v := dests[0].(FieldBinder).Value(); model.TestID != v.(string) {
		t.Errorf("bind unmatch (TestID) : expect is %v but result is %v", v, model.TestID)
	}

	if v := dests[1].(FieldBinder).Value(); !reflect.DeepEqual(model.Date.Time, v.(*NullTime).Time) {
		t.Errorf("bind unmatch (Date) : expect is %v but result is %v", v, model.Date)
	}

	if v := dests[2].(FieldBinder).Value(); model.Age.Int64 != v.(*NullInt64).Int64 {
		t.Errorf("bind unmatch (Age) : expect is %v but result is %v", v, model.Age)
	}
}

func TestBind_UnmatchLengthOfColumnAndBinder(t *testing.T) {
	model := &struct {
		TestID string     `bindmap:"col:id,type:varchar"`
		Date   *NullTime  `bindmap:"col:date_time,type:datetime"`
		Age    *NullInt64 `bindmap:"col:the_age,type:int"`
	}{}

	//dests各要素への値セットはsql.Scan()による値読込みを表す
	var dests = make([]interface{}, 0)
	if w := newNullStringWrapper(); true {
		w.Scan("999999")
		dests = append(dests, w)
	}
	if w := newNullStringWrapper(); true {
		w.Scan("!!!!!!!!")
		dests = append(dests, w)
	}
	if w := newNullTimeWrapper(); true {
		w.Scan("2017-1-16 11:22:33")
		dests = append(dests, w)
	}
	if w := newNullInt64Wrapper(); true {
		w.Scan(34)
		dests = append(dests, w)
	}

	fieldIdxs := []int{0, -1, 1, 2}

	err := bind(dests, fieldIdxs, model)
	if err != nil {
		t.Errorf("bind error : %+v", err)
		return
	}

	if v := dests[0].(FieldBinder).Value(); model.TestID != v.(string) {
		t.Errorf("bind unmatch (TestID) : expect is %v but result is %v", v, model.TestID)
	}

	if v := dests[2].(FieldBinder).Value(); !reflect.DeepEqual(model.Date.Time, v.(*NullTime).Time) {
		t.Errorf("bind unmatch (Date) : expect is %v but result is %v", v, model.Date)
	}

	if v := dests[3].(FieldBinder).Value(); model.Age.Int64 != v.(*NullInt64).Int64 {
		t.Errorf("bind unmatch (Age) : expect is %v but result is %v", v, model.Age)
	}
}

func TestBind_NULLField(t *testing.T) {
	model := &struct {
		TestID string     `bindmap:"col:id,type:varchar"`
		Date   *NullTime  `bindmap:"col:date_time,type:datetime"`
		Age    *NullInt64 `bindmap:"col:the_age,type:int"`
	}{}

	//dests各要素への値セットはsql.Scan()による値読込みを表す
	var dests = make([]interface{}, 0)
	if w := newNullStringWrapper(); true {
		w.Scan(nil)
		dests = append(dests, w)
	}
	if w := newNullTimeWrapper(); true {
		w.Scan(nil)
		dests = append(dests, w)
	}
	if w := newNullInt64Wrapper(); true {
		w.Scan(nil)
		dests = append(dests, w)
	}

	fieldIdxs := []int{0, 1, 2}

	err := bind(dests, fieldIdxs, model)
	if err != nil {
		t.Errorf("bind error : %+v", err)
		return
	}

	if v := dests[0].(FieldBinder).Value(); model.TestID != v.(string) {
		t.Errorf("bind unmatch (TestID) : expect is %v but result is %v", v, model.TestID)
	}

	if v := dests[1].(FieldBinder).Value(); !reflect.DeepEqual(model.Date, v) {
		t.Errorf("bind unmatch (Date) : expect is %v but result is %v", v, model.Date)
	}

	if v := dests[2].(FieldBinder).Value(); model.Age != v {
		t.Errorf("bind unmatch (Age) : expect is %v but result is %v", v, model.Age)
	}
}
