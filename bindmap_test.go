package gomodelbinding

import (
	"testing"
)

func TestCnvCamelToSnake(t *testing.T) {
	caseAndExpends := []struct {
		camel, snake string
	}{
		{"RecordID", "record_id"},
		{"RecordId", "record_id"},
		{"aRecordID", "a_record_id"},
		{"recordIdAND", "record_id_a_n_d"},
		{"recordIdAnd", "record_id_and"},
		{"record_id", "record_id"},
		{"ID", "id"},
		{"Weight", "weight"},
		{"JoggingResultMSeconds", "jogging_result_m_seconds"},
	}

	for i, cae := range caseAndExpends {
		if ret := cnvCamelToSnake(cae.camel); cae.snake != ret {
			t.Errorf("No.%d expend is %v but result is %v \r\n", i+1, cae.snake, ret)
		}
	}
}

func TestCnvSnakeToCamel(t *testing.T) {
	caseAndExpends := []struct {
		snake, camel string
	}{
		{"record_id", "RecordId"},
		{"a_record_id", "ARecordId"},
		{"record_id_and", "RecordIdAnd"},
		{"RecordID", "Recordid"},
	}

	for i, cae := range caseAndExpends {
		if ret := cnvSnakeToCamel(cae.snake); cae.camel != ret {
			t.Errorf("No.%d expend is %v but result is %v \r\n", i+1, cae.camel, ret)
		}
	}
}
